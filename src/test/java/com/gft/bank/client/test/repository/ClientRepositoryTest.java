package com.gft.bank.client.test.repository;

import static org.junit.Assert.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.gft.bank.client.ClientApplication;
import com.gft.bank.client.model.Client;
import com.gft.bank.client.repository.ClientRepository;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientApplication.class)
@WebAppConfiguration
public class ClientRepositoryTest {

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	TestRespository testRespository;


	@Test
	@DisplayName("Should find client by Nick ")
	public void shouldFindclientByNick() {
		Client stub = getStubclient();
		//clientRepository.save(stub);
		//Client newclient = testRespository.add(getStubclient());
		//Client found = clientRepository.findById(newclient.getId()).get();
		//assertEquals(stub.getId(), found.getId());
	}

	/**
	 * 
	 * @return Create a new client with id 
	 */
	private Client getStubclient() {
		Client client = new Client();
		client.setId(11L);		
		return client;
	}

	@Component
	static  class TestRespository{
		@Autowired
		private ClientRepository clientrRepository;

		/**
		 * @return list clients 
		 */
		 public Iterable<Client> findall(){
			 return clientrRepository.findAll();
		 }

		 /**
		  * 
		  * @param client
		  * @return A client created
		  */
		 public Client add(Client client){
			 return clientrRepository.save(client);
		 }

		/**
		 * 
		 * @param Id from client created
		 * @return client finded 
		 */
		 public Client findById(Long Id){
			 return clientrRepository.findById(Id).get();
		 }

	} 
}
