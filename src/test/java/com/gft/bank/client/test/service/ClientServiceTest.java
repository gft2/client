package com.gft.bank.client.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gft.bank.client.model.Client;
import com.gft.bank.client.repository.ClientRepository;
import com.gft.bank.client.service.ClientServiceImpl;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import org.springframework.transaction.annotation.Propagation;

@RunWith(SpringRunner.class)
public class ClientServiceTest {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@InjectMocks
	private ClientServiceImpl storeService;


	@MockBean(name = "clientRepository")
	private ClientRepository clientRepository;

	@Before
	public void setup() {
		initMocks(this);
	}

	
	@Test
	@DisplayName("Should get tostring object client")
	public void shoulGetToStingObject(){
		Client client = new Client();
		client.setId(11L);
		//assertEquals(client.toString().toCharArray().length,115);
	}

	@Test
	@DisplayName("Should is equals object")
	public void shouldIsEqualObject(){
		Client client = new Client();
		client.setId(11L);
		//assertEquals(client.equals(client),true);
	}


	@Test
	@DisplayName("Should is not equals object")
	public void shouldIsNotEqualObject(){
		Client client = new Client();
		Client client2 = new Client();
		client.setId(11L);
		//assertEquals(client.equals(client2),false);
	}

	@Test
	@DisplayName("Should get hastcode client")
	public void shouldGetHastcodeclient(){
		Client client = new Client();
		client.setId(11L);
		//assertEquals(client.hashCode(),1213507831);
	}


	@Test
	@DisplayName("Should create client with given user")
	public void shouldCreateclientWithGivenUser() {
		Client client = new Client();
		client.setId(11L);
		
		Client resclient=storeService.create(client);
		//assertEquals(resclient.getPetId(),client.getPetId());
		//assertEquals(resclient.getId() ,client.getId());
		//assertEquals(resclient.isComplete() ,true);

		//log.info("asotmm "+resclient.complete(true));

		//verify(clientRepository, times(1)).save(client);
	}

	@Test
	@DisplayName("Should delete client with given user")
	public void shouldDeleteclientWithGivenUser(){
		Client client = new Client();
		Client resclient =storeService.create(client);
		//storeService.deleteclient(resclient);
		//assertEquals(resclient.getPetId(), null);
	}


	@Test
	@DisplayName("Should get all clients null")
	public void shouldGetallclientsNull(){
		List<Client> stores = storeService.findAll();
		//assertEquals(stores, null);
	}

	@Test
	@DisplayName("Should get all clients")
	public void shouldGetAllclients(){
		createclientbyId();
		List<Client> stores = storeService.findAll();
        List<String> actual = Arrays.asList("a", "b", "c");
		//assertEquals(actual.size(),3);

	}

	private Client createclientbyId() {		
		Client client = new Client();
		client.setId(11L);		
		return  storeService.create(client);
	}
}
