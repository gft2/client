package com.gft.bank.client.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gft.bank.client.model.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

	Optional<Client> findById(Long Id);
	
	
}
