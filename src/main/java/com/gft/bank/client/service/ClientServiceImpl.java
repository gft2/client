package com.gft.bank.client.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.gft.bank.client.model.Client;
import com.gft.bank.client.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ClientRepository clientRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client findById(Long id) {
		Optional<Client> client = clientRepository.findById(id);
		Assert.notNull(client, "can't find client with name " + client.get().getId());
		client.get().setId(client.get().getId());
		return client.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client create(Client client) {

		Client existing = clientRepository.findById(client.getId()).orElse(null);

		Assert.isNull(existing, "client already exists: " + client.getId());
		clientRepository.save(client);

		log.info("new client has been created: " + client.getId());

		return client;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveChanges(Client update) {
		Optional<Client> client = clientRepository.findById(update.getId());
		Assert.notNull(client, "can't find client with name " + update.getId());
		client.get().setId(update.getId());
		client.get().setFirst_name(update.getFirst_name());
		client.get().setLast_name(update.getLast_name());
		client.get().setEmail(update.getEmail());
		client.get().setGenere(update.getGenere());
		
		clientRepository.save(client.get());
		log.debug("client {} changes has been saved", update.getId());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Client> findAll() {
		Iterable<Client> findAll = clientRepository.findAll();
		List<Client> myList = new ArrayList<Client>();
		findAll.forEach(item -> myList.add(item) );
		return myList;
	}

	@Override
	public void deleteClient(Client client) {
		clientRepository.delete(client);
	}

	

}
