package com.gft.bank.client.service;

import java.util.List;

import com.gft.bank.client.model.Client;



public interface ClientService {

	/**
	 * Finds client by given name
	 *
	 * @param clientId
	 * @return found client
	 */
	Client findById(Long  Id);

	/**
	 * Checks if client with the same name already exists
	 * Invokes Auth Service user creation
	 * Creates new client with default parameters
	 *
	 * @param client
	 * @return created client
	 */
	Client create(Client client);

	/**
	 * Validates and applies incoming client updates
	 * Invokes Statistics Service update
	 *
	 * @param name
	 * @param update
	 */
	void saveChanges(Client update);

	/**
	 * 
	 * @return
	 */
	List<Client> findAll();

	void deleteClient(Client client);


}
